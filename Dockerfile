#
# Apache Archiva Standalone
# https://archiva.apache.org/docs/2.2.10/adminguide/standalone.html
#
# $ docker run -itd --name archiva -p 8080:8080 jezeklu/archiva
#

FROM openjdk:8-jdk

ENV ARCHIVA_VERSION=2.2.10

LABEL name="jezeklu/archiva" \
      maintainer="Lukas Jezek <jezeklu@fastmail.com>" \
      version="${ARCHIVA_VERSION}" \
      url="https://gitlab.com/jezeklu/archiva-docker" \
      run='docker run -itd --name archiva -p 8080:8080 jezeklu/archiva' \
      summary="Apache Archiva Image, based on openjdk"

### Setup user for build execution and application runtime
ENV APP_ROOT=/opt/app
ENV PATH=${APP_ROOT}/bin:${PATH} HOME=${APP_ROOT} ARCHIVA_BASE=${APP_ROOT}/archiva-base

RUN mkdir -p ${APP_ROOT} \
      && wget -q http://www-eu.apache.org/dist/archiva/${ARCHIVA_VERSION}/binaries/apache-archiva-${ARCHIVA_VERSION}-bin.zip \
      && unzip -q apache-archiva-${ARCHIVA_VERSION}-bin.zip -d /tmp \
      && cp -frp /tmp/apache-archiva*/* ${APP_ROOT}/ \
      && rm -fr /tmp/apache-archiva* apache-archiva-${ARCHIVA_VERSION}-bin.zip \
      && mkdir -p ${ARCHIVA_BASE}/logs ${ARCHIVA_BASE}/data ${ARCHIVA_BASE}/temp ${ARCHIVA_BASE}/repositories \
      && cp -frp ${APP_ROOT}/conf ${ARCHIVA_BASE}/

COPY resources/ ${APP_ROOT}/bin/
RUN chmod -R u+x ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd

### Containers should NOT run as root as a good practice
USER 10001
WORKDIR ${ARCHIVA_BASE}

ENTRYPOINT [ "uid_entrypoint" ]
VOLUME ${ARCHIVA_BASE}
EXPOSE 8080
CMD start-archiva
