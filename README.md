# archiva-docker
Apache Archiva image built inspired by https://github.com/jefferyb/apache-archiva.

Docker hub: https://hub.docker.com/r/jezeklu/archiva


## How run it
### Docker
```
# Quick lunch
$ docker run -itd --name apache-archiva -p 8080:8080 jezeklu/archiva

# Setup persistent volume
$ docker run -itd --name apache-archiva -p 8080:8080 -v /opt/apache-archiva:/opt/app/archiva-base jezeklu/archiva
```

### Docker Compose
```
services:
  archiva:
    image: jezeklu/archiva:${ARCHIVA_VERSION}
    container_name: archiva-repo
    restart: always
    volumes:
      - /my-persistent-storage/archiva/data:/opt/app/archiva-base
    ports:
      - "${ARCHIVA_HTTP_PORT}:8080"
```



